/*
 c program:
 --------------------------------
  1. draws Mandelbrot set for Fc(z)=z*z +c
  using Mandelbrot algorithm ( boolean escape time )
 -------------------------------
 2. technique of creating ppm file is  based on the code of Claudio Rocchini
 http://en.wikipedia.org/wiki/Image:Color_complex_plot.jpg
 create 24 bit color graphic file ,  portable pixmap file = PPM
 see http://en.wikipedia.org/wiki/Portable_pixmap
 to see the file use external application ( graphic viewer)
  */
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

/* CONFIGURATION */

// size = width = height of output file (in pixels)
#define size 6000
//number of threads, max 8:
//  ! moved below  !
// repeat calculations for more accurate results:
#define numberOfRepetitions 5

// define order, R-G-B
#define redIndex 0
#define greenIndex 1
#define blueIndex 2

// function declarations:
double calculateMandelbrot(unsigned char colors[][3]);

// array containing colors to be used by each thread
// calculated here to avoid overhead while measuring calculation time

// array containing result output image
static unsigned char output[size][size][3];

// counters for number of iterations for each thread
unsigned long long int iterationCounters[8] = {0};

int main(int argc, char *argv[])
{
    const int numberOfThreads = atoi(argv[1]);
    unsigned char colors[numberOfThreads][3];
    /* color component ( R or G or B) is coded from 0 to 255 */
    /* it is 24 bit color RGB file */
    const int MaxColorComponentValue = 255;

    double timeSum = 0.0;
    double averageTime;

    omp_set_num_threads(numberOfThreads);

    FILE *resultFile;
    char *filename = "out.ppm";
    char *comment = "# "; /* comment should start with # */
    /*create new file,give it a name and open it in binary mode  */
    resultFile = fopen(filename, "wb"); /* b -  binary mode */
    /*write ASCII header to the file*/
    fprintf(resultFile, "P6\n %s\n %d\n %d\n %d\n", comment, size, size, MaxColorComponentValue);

    /* generate <numberofThreads> shades of green */
    for (int i = 0; i < numberOfThreads; i++)
    {
        colors[i][redIndex] = 15 + i * 5;
        colors[i][greenIndex] = 15 + i * 30;
        colors[i][blueIndex] = 15 + i * 5;
    }

    for (int i = 0; i < numberOfRepetitions; i++)
    {
        timeSum = calculateMandelbrot(colors);
    }

    averageTime = timeSum / numberOfRepetitions;

    // print results
    printf("\nNumber of threads used: %d\n", numberOfThreads);
    printf("Average execution time: %f \n", averageTime);
    printf("Thread ID\titerations\n\n");
    for (int m = 0; m < numberOfThreads; m++)
    {
        printf(" %d\t\t%llu\n", m, iterationCounters[m]);
    }
    fwrite(output, 1, sizeof(output), resultFile);
    fclose(resultFile);

    return 0;
}

double calculateMandelbrot(unsigned char colors[][3])
{
    /* screen ( integer) coordinate */
    int iX, iY;
    int iXmax = size;
    int iYmax = size;

    double CxMin = -2.5;
    double CxMax = 1.5;
    double CyMin = -2.0;
    double CyMax = 2.0;

    /* world ( double) coordinate = parameter plane*/
    double Cx, Cy;

    /* */
    double PixelWidth = (CxMax - CxMin) / iXmax;
    double PixelHeight = (CyMax - CyMin) / iYmax;

    /* bail-out value , radius of circle ;  */
    double EscapeRadius = 2;

    double ER2 = EscapeRadius * EscapeRadius;

    /* Z=Zx+Zy*i  ;   Z0 = 0 */
    double Zx, Zy;
    double Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
    /*  */
    int Iteration;
    int IterationMax = 200;

    double time;
    int threadID;
    time = omp_get_wtime();

#pragma omp parallel private(threadID)
    {
        //printf("%d",omp_get_num_threads());
        threadID = omp_get_thread_num();

#pragma omp for private(Zx, Zy, Zx2, Zy2, Cy, iX, Iteration) schedule(dynamic, 1)
        for (iY = 0; iY < iYmax; iY++)
        {

            //printf("%d\n",threadID);
            Cy = CyMin + iY * PixelHeight;
            if (fabs(Cy) < PixelHeight / 2)
                Cy = 0.0; /* Main antenna */
            for (iX = 0; iX < iXmax; iX++)
            {
                Cx = CxMin + iX * PixelWidth;
                /* initial value of orbit = critical point Z= 0 */
                Zx = 0.0;
                Zy = 0.0;
                Zx2 = Zx * Zx;
                Zy2 = Zy * Zy;
                /* */
                for (Iteration = 0; Iteration < IterationMax && ((Zx2 + Zy2) < ER2); Iteration++)
                {
                    iterationCounters[threadID]++;
                    Zy = 2 * Zx * Zy + Cy;
                    Zx = Zx2 - Zy2 + Cx;
                    Zx2 = Zx * Zx;
                    Zy2 = Zy * Zy;
                };
                /* compute  pixel color (24 bit = 3 bytes) */
                if (Iteration == IterationMax)
                { /*  interior of Mandelbrot set = grey, brightness depending on threadID */
                    output[iY][iX][redIndex] = threadID * 5;
                    output[iY][iX][greenIndex] = threadID * 5;
                    output[iY][iX][blueIndex] = threadID * 5;
                }
                else
                {
                    /* exterior of Mandelbrot set = white */
                    output[iY][iX][redIndex] = colors[threadID][redIndex];     /* Red*/
                    output[iY][iX][greenIndex] = colors[threadID][greenIndex]; /* Green */
                    output[iY][iX][blueIndex] = colors[threadID][blueIndex];   /* Blue */
                };
            }
        }
    }
    time = omp_get_wtime() - time;
    return time;
}
